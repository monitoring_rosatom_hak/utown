<?php

namespace App\Entity;

use App\Repository\FireRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="""issues"".""fire""")
 * @ORM\Entity(repositoryClass=FireRepository::class)
 */
class Fire
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $lat;

//    /**
//     * @ORM\Column(type="float")
//     */
//    private $float;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dot_name;

    /**
     * @ORM\Column(type="float")
     */
    private $lon;


    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

//    public function getFloat(): ?float
//    {
//        return $this->float;
//    }
//
//    public function setFloat(float $float): self
//    {
//        $this->float = $float;
//
//        return $this;
//    }

    public function getDotName(): ?string
    {
        return $this->dot_name;
    }

    public function setDotName(?string $dot_name): self
    {
        $this->dot_name = $dot_name;

        return $this;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function setLon(float $lon): self
    {
        $this->lon = $lon;

        return $this;
    }
}
