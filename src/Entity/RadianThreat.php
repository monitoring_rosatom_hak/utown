<?php

namespace App\Entity;

use App\Repository\RadianThreatRepository;
use Doctrine\ORM\Mapping as ORM;

use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="""issues"".""radian_treat""")
 * @ORM\Entity(repositoryClass=RadianThreatRepository::class)
 */
class RadianThreat
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @ORM\Column(type="float")
     */
    private $lon;

    /**
     * @ORM\Column(type="float")
     */
    private $dosa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dot_name;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function setLon(float $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    public function getDosa(): ?float
    {
        return $this->dosa;
    }

    public function setDosa(float $dosa): self
    {
        $this->dosa = $dosa;

        return $this;
    }

    public function getDotName(): ?string
    {
        return $this->dot_name;
    }

    public function setDotName(?string $dot_name): self
    {
        $this->dot_name = $dot_name;

        return $this;
    }
}
