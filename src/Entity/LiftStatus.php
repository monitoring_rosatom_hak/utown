<?php

namespace App\Entity;

use App\Repository\LiftStatusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="""helpers"".""lift_status""")
 * @ORM\Entity(repositoryClass=LiftStatusRepository::class)
 */
class LiftStatus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
