<?php

namespace App\Entity;

use App\Repository\LiftRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Table(name="""issues"".""lift""")
 * @ORM\Entity(repositoryClass=LiftRepository::class)
 */
class Lift
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var UuidInterface
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @ORM\Column(type="float")
     */
    private $lon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dot_name;

    /**
     * @ORM\Column(type="smallint")
     */
    private $floor;

    /**
     * @ORM\ManyToOne(targetEntity=LiftStatus::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function setLon(float $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    public function getDotName(): ?string
    {
        return $this->dot_name;
    }

    public function setDotName(?string $dot_name): self
    {
        $this->dot_name = $dot_name;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function getStatus(): ?LiftStatus
    {
        return $this->status;
    }

    public function setStatus(?LiftStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
}
