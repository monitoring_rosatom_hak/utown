<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531141702 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE "issues"."incident" (id UUID NOT NULL, type_id UUID NOT NULL, lat DOUBLE PRECISION NOT NULL, lon DOUBLE PRECISION NOT NULL, dot_name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E0A902F8C54C8C93 ON "issues"."incident" (type_id)');
        $this->addSql('COMMENT ON COLUMN "issues"."incident".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "issues"."incident".type_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "helpers"."incident_class" (id UUID NOT NULL, value VARCHAR(120) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN "helpers"."incident_class".id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "issues"."incident" ADD CONSTRAINT FK_E0A902F8C54C8C93 FOREIGN KEY (type_id) REFERENCES "helpers"."incident_class" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "issues"."incident" DROP CONSTRAINT FK_E0A902F8C54C8C93');
        $this->addSql('DROP TABLE "issues"."incident"');
        $this->addSql('DROP TABLE "helpers"."incident_class"');
    }
}
