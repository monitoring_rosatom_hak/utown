<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200531103544 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA helpers');
        $this->addSql('CREATE SEQUENCE "helpers"."lift_status_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "issues"."lift" (id UUID NOT NULL, status_id INT NOT NULL, lat DOUBLE PRECISION NOT NULL, lon DOUBLE PRECISION NOT NULL, dot_name TEXT DEFAULT NULL, float SMALLINT NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_80E0B766BF700BD ON "issues"."lift" (status_id)');
        $this->addSql('COMMENT ON COLUMN "issues"."lift".id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "helpers"."lift_status" (id INT NOT NULL, value VARCHAR(120) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE "issues"."lift" ADD CONSTRAINT FK_80E0B766BF700BD FOREIGN KEY (status_id) REFERENCES "helpers"."lift_status" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "issues"."lift" DROP CONSTRAINT FK_80E0B766BF700BD');
        $this->addSql('DROP SEQUENCE "helpers"."lift_status_id_seq" CASCADE');
        $this->addSql('DROP TABLE "issues"."lift"');
        $this->addSql('DROP TABLE "helpers"."lift_status"');
    }
}
