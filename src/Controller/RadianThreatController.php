<?php

namespace App\Controller;

use App\Repository\RadianThreatRepository;
use App\Entity\RadianThreat;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RadianThreatController
 * @package App\Controller
 * @Route("/issues/radian")
 */
class RadianThreatController extends AbstractController
{
//    /**
//     * @Route("/radian/threat", name="radian_threat")
//     */
//    public function index()
//    {
//        return $this->render('radian_threat/index.html.twig', [
//            'controller_name' => 'RadianThreatController',
//        ]);
//    }



    /**
     * @Route("/")
     * @param RadianThreatRepository $radianThreatRepository
     * @return mixed
     */
    public  function list(/*RadianThreatRepository $radianThreatRepository*/ Connection $connection){
////        $repository = $this->getDoctrine()->getRepository(RadianThreatRepository::class);
////        $data = $repository->findAll();
//        $response = $radianThreatRepository->findAll();
//        return $this->json($response);
////        return $this->json($data);
///
        $data = $connection->fetchAll("SELECT id, lat, lon, dosa, dot_name, created_at, updated_at, deleted_at
	FROM issues.radian_treat order by created_at desc ");

        return $this->json($data);
///
    }

    /**
     * @Route("/new")
     * @param Request $request
     * @param Connection $connection
     * @return Response
     */
    public function new(Request $request, Connection $connection){
        $lat = $request->get('lat',0);
        $lon = $request->get('lon',0);
        $dosa = $request->get('dosa',5);
        $dot_name = $request->get('dot_name');

        $entityManager = $this->getDoctrine()->getManager();
        $radian = new RadianThreat();
        $radian->setLat($lat);
        $radian->setLon($lon);
        $radian->setDosa($dosa);
        $radian->setDotName($dot_name);

        $entityManager->persist($radian);

        $entityManager->flush();

        return new Response($radian->getId());

    }
//

}
