<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class IssuesController
 * @package App\Controller
 * @Route("/issues")
 */
class IssuesController extends AbstractController
{
    /**
     * @Route("/", name="issues_dashboard")
     */
    public function index()
    {
        return $this->render('issues/index.html.twig', [
            'controller_name' => 'IssuesController',
        ]);
    }
}
