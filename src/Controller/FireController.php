<?php

namespace App\Controller;

use App\Entity\Fire;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FireController
 * @package App\Controller
 * @Route("/issues/fire")
 */

class FireController extends AbstractController
{

//    /**
//     * @Route("/fire", name="fire")
//     */
//    public function index()
//    {
//        return $this->render('fire/index.html.twig', [
//            'controller_name' => 'FireController',
//        ]);
//    }

    /**
     * @Route("/")
     * @param Connection $connection
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function list(Connection $connection){
        $data = $connection->fetchAll("select * from issues.fire");
        return $this->json($data);
    }

    /**
     * @Route("/new")
     * @param Request $request
     * @param Connection $connection
     * @return Response
     */
    public function new(Request $request, Connection $connection){
        $lat = $request->get('lat',0);
        $lon = $request->get('lon',0);

        $dot_name = $request->get('dot_name');

        $entityManager = $this->getDoctrine()->getManager();
        $fire = new Fire();
        $fire->setLat($lat);
        $fire->setLon($lon);
        $fire->setDotName($dot_name);

        $entityManager->persist($fire);
        $entityManager->flush();

        return new Response($fire->getId());


    }
}
