<?php

namespace App\Controller;

use App\Entity\Incident;
use App\Entity\IncidentClass;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IncidentController
 * @package App\Controller
 * @Route("/issues/incident")
 */
class IncidentController extends AbstractController
{
    /**
     * @Route("/")
     * @param Connection $connection
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function list(Connection $connection){
        $data = $connection->fetchAll("SELECT incident.id, incident_class.value as type, type_id, lat, lon, dot_name, incident.created_at
	FROM issues.incident
	left join helpers.incident_class on (incident_class.id=incident.type_id)");
        return $this->json($data);
    }


    /**
     * @Route("/new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request){
        $lat = $request->get('lat',0);
        $lon = $request->get('lon',0);
        $dot_name = $request->get('dot_name','  ');

        $type_str = $request->get('type','935d45fa-d7b6-407a-9ca4-7b94266a2a9a');
        $entityManager = $this->getDoctrine()->getManager();
        $type = $entityManager->getRepository(IncidentClass::class)->find($type_str);

        $incident = new Incident();
        $incident->setLat($lat);
        $incident->setLon($lon);
        $incident->setDotName($dot_name);
        $incident->setType($type);

        $entityManager->persist($incident);
        $entityManager->flush();

        return new Response($incident->getId());
    }

}
