<?php

namespace App\Controller;

use App\Entity\Lift;
use App\Entity\LiftStatus;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LiftController
 * @package App\Controller
 * @Route("/issues/lift")
 */
class LiftController extends AbstractController
{

    /**
     * @Route("/")
     * @param Request $request
     * @param Connection $connection
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function list(Request $request, Connection $connection){
        $data = $connection->fetchAll("SELECT lift.id, status_id, lift_status.value as status,
lat, lon, dot_name, floor, created_at
	FROM issues.lift
	left join helpers.lift_status on (lift_status.id=lift.status_id)");

        return $this->json($data);
    }

    /**
     * @Route("/new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request){
        $lat = $request->get('lat',0);
        $lon = $request->get('lon',0);
        $dot_name = $request->get('dot_name');
        $status = $request->get('status',1);
        $floar = $request->get('floar',0);

        $entityManager = $this->getDoctrine()->getManager();

        $lift_status = $entityManager->getRepository(LiftStatus::class)->find($status);

        $lift = new Lift();
        $lift->setLat($lat);
        $lift->setLon($lon);
        $lift->setStatus($lift_status);
        $lift->setFloat($floar) ;

        $entityManager->persist($lift);
        $entityManager->flush();

        return new Response($lift->getId());
    }

}
