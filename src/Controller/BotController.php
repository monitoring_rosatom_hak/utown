<?php

namespace App\Controller;

use App\Entity\TelegramClient;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Symfony\Flex\Configurator\EnvConfigurator;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;


//use Telegram\Bot\Api;

class BotController extends AbstractController
{
    /**
     * @var string
     */
    private $bot_token;

    public function __construct()
    {
        $this->bot_token = $_ENV['TELEGRAM_BOT_TOCKEN'];
    }

    /**
     * @Route("/bot", name="bot")
     */
    public function bot_st(Request $request,LoggerInterface $logger)
    {
        $result = json_decode($request->getContent(), true);

        $logger->info(json_encode($result));

        if (isset($result["callback_query"])){
            $bot = new \TelegramBot\Api\BotApi($this->bot_token);
            $chatId = $result["callback_query"]["message"]["chat"]["id"];
            $data_q = $result["callback_query"]["data"];

            $type_in = "";
            switch ($data_q){
                case "subscribe_fire" :
                    $type_in="сведений о пожарах";
                    break;
                case "subscribe_radian" :
                    $type_in="повышении уровня радиации";
                    break;
                case "subscribe_lift" :
                    $type_in="инцидентах с лифтами";
                    break;
                case "subscribe_incident" :
                    $type_in="происшествиях";
                    break;

            }

            $entityManager = $this->getDoctrine()->getManager();
            $telegram_user = $entityManager->getRepository(TelegramClient::class)->find($chatId);
            if ($telegram_user){
                $im = $telegram_user->getFirstName();

                $bot->sendMessage($chatId, "$im, Вы подписались  на получение уведомлений о *$type_in* ","Markdown");
            }



        }


        if (isset($result["message"]["text"])) {
            $text = $result["message"]["text"]; // Текст сообщения
            $chatId = $result["message"]["chat"]["id"]; // Уникальный идентификатор пользователя
            $telegramId = $result["message"]["from"]["id"]; //Id пользователя в телеграме
            $bot = new \TelegramBot\Api\BotApi($this->bot_token);
//            //$bot = new \TelegramBot\Api\Client($token);
//
            switch ($text){
                case "/start" : {

                    $entityManager = $this->getDoctrine()->getManager();

                    $telegram_user = $entityManager->getRepository(TelegramClient::class)->find($result["message"]["from"]["id"]);
                    if (!$telegram_user){
                        $telegram_user = new TelegramClient($result["message"]["from"]["id"]);
                        $telegram_user->setFirstName($result["message"]["from"]["first_name"]);
                        if(isset($result["message"]["from"]["last_name"]))
                            $telegram_user->setLastName($result["message"]["from"]["last_name"]);
                        else
                            $telegram_user->setLastName(" ");
                        if(isset($result["message"]["from"]["username"]))
                            $telegram_user->setUsername($result["message"]["from"]["username"]);
                        else
                            $telegram_user->setUsername("");

                        $telegram_user->setChat($chatId);
                        $entityManager->persist($telegram_user);
                        $entityManager->flush();
                    }

                    $name = $telegram_user->getFirstName();

                    $this->startBotHello($chatId,$name);
                    $this->sendHelp($chatId);
                    break;
                }
                case "/help" : {
                    $this->sendHelp($chatId);
                    break;
                }
                case "/status" : {
                    $this->sendStatus($chatId);
                    break;
                }
                case "/subscribe" : {
                    $this->sendSubscribeVariant($chatId);
                    break;
                }
                case "subscribe_test" : {
                    $bot = new \TelegramBot\Api\BotApi($this->bot_token);
                    $bot->sendMessage($chatId, "Вы подписались  на получение сестовых уведомлений","Markdown");
                }

            }

        }

        //$logger->info("TEST BOT ".$text.$request->getClientIp() );

        return new Response("bot_test");
    }

    private function startBotHello(int $chatId,string $name){
        $bot = new \TelegramBot\Api\BotApi($this->bot_token);
        $bot->sendMessage($chatId, "$name, приветствуем Вас в чат-боте *Умный Город* ","Markdown");
    }

    private function sendHelp(int $chatId){
        $bot = new \TelegramBot\Api\BotApi($this->bot_token);
        $bot->sendMessage($chatId,"Для взаимодействия с ботом используются следующие команды:
                    /help - помощь
                    /status - получение информации об обстановке в округе 
                    /subscribe - подписка на уведомления
                    /site - указание мест для получения уведомлений");
    }

    private function sendStatus(int $chatId){
        $bot = new \TelegramBot\Api\BotApi($this->bot_token);
        $bot->sendMessage($chatId,"Ситуация в округе: ");
    }

    private function sendSubscribeVariant(int $chatId){
        $bot = new \TelegramBot\Api\BotApi($this->bot_token);

//        $keyboard = new \TelegramBot\Api\Types\ReplyKeyboardMarkup(array(array("one", "two", "three")), true); // true for one-time keyboard
//
//        $bot->sendMessage($chatId, "messageText", null, false, null, $keyboard);


        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
            [
                [
                    ['text' => hex2bin('F09F94A5').'Пожары', 'callback_data' =>'subscribe_fire' ],
                    ['text' => 'Радиация', 'callback_data' =>'subscribe_radian' ],

                ],
                [
                    ['text' => 'Лифты', 'callback_data' =>'subscribe_lift' ],
                    ['text' => hex2bin('F09F9A94').'Происшествия', 'callback_data' =>'subscribe_incident' ]
                ]
            ]
        );

        $bot->sendMessage($chatId, "Выберите необходимую подписку", null, false, null, $keyboard);


    }
}
