<?php

namespace App\Repository;

use App\Entity\Lift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lift[]    findAll()
 * @method Lift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LiftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lift::class);
    }

    // /**
    //  * @return Lift[] Returns an array of Lift objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lift
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
