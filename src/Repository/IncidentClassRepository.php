<?php

namespace App\Repository;

use App\Entity\IncidentClass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IncidentClass|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentClass|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentClass[]    findAll()
 * @method IncidentClass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentClass::class);
    }

    // /**
    //  * @return IncidentClass[] Returns an array of IncidentClass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IncidentClass
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
