<?php

namespace App\Repository;

use App\Entity\Fire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fire[]    findAll()
 * @method Fire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fire::class);
    }

    // /**
    //  * @return Fire[] Returns an array of Fire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fire
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
