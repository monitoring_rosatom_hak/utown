<?php

namespace App\Repository;

use App\Entity\TelegramClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TelegramClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method TelegramClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method TelegramClient[]    findAll()
 * @method TelegramClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TelegramClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TelegramClient::class);
    }

    // /**
    //  * @return TelegramClient[] Returns an array of TelegramClient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TelegramClient
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
