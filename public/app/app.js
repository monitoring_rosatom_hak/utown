
var app = {

    init : function () {

        webix.storage.local.clear();
        webix.i18n.setLocale('ru-RU');
        webix.Date.startOnMonday = true;

        webix.ui(
            mainWND
        )

        $$('sidebar').select('issues_statistic')

    },

    
    menu_selected : function (id) {
        switch (id) {
            case 'issues_statistic' :
                app.issues_statistic()
                break
            case 'issues' :
                app.issues()
                break
            case 'radian' :
                app.radian()
                break
            case 'fire' :
                app.fire()
                break
            case 'lift' :
                app.lift()
                break
            case 'incident' :
                app.incident()
                break
            case 'notifers' :
                app.notifers()
                break

        }
    },


    issues_statistic : function(){

        if (!$$('dashboard')){


            $$('view').addView({
                id : 'dashboard',
                view : 'dashboard',

            })
        }

        $$('dashboard').show()

    },

    radian : function(){
        if(!$$('radian')){
            $$('view').addView({
                id : 'radian',
                view : 'radian',

            })
        }
        $$('radian').load()
        $$('radian').show()
    },

    fire : function(){
        if(!$$('fire')){
            $$('view').addView({
                id : 'fire',
                view : 'fire',

            })
        }
        $$('fire').load()
        $$('fire').show()
    },

    lift : function(){
        if(!$$('lift')){
            $$('view').addView({
                id : 'lift',
                view : 'lift',

            })
        }
        $$('lift').load()
        $$('lift').show()
    },

    incident : function(){
        if(!$$('incident')){
            $$('view').addView({
                id : 'incident',
                view : 'incident',

            })
        }
        $$('incident').load()
        $$('incident').show()
    },

    issues : function(){

    },

    notifers : function () {

    }
}