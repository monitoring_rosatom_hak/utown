
var menu_data = [

    {id: "issues_statistic", icon : "fa fa-chart-bar", value: "Мониторинг"},
    {id: "issues", icon: "fas fa-radiation-alt ", value: "События",
        data : [
            {id : 'radian',value:'Радиация', icon : 'fas fa-radiation-alt'},
            {id : 'fire', value:'Пожары', icon : 'fas fa-fire-extinguisher' },
            {id : 'lift', value:'Лифты',}
        ]
    },
    {id: "incident", icon: "fas fa-exclamation ", value:'Происшествия'},
    {id: "notifers", icon: 'fas fa-users', value: 'Подписчики' }


]

var mainWND = {
    rows: [
        {
            view: "toolbar", css: "webix_dark", padding: 3, elements: [
                {
                    view: "button", type: "icon", icon: "fa fa-bars" /*"mdi mdi-menu"*/,
                    width: 37, align: "left", css: "app_button", click: function () {
                        $$("sidebar").toggle();
                    }
                },
                {view: "label", label: "Мой.Город - Панель оператора"},
                {},
            ]
        },
        { cols:[
                {
                    view: "sidebar",
                    id: 'sidebar',
                    css:"webix_dark",
                    collapsed:true,
                    data: menu_data,
                    on:{
                        onAfterSelect: function(id){
                            app.menu_selected(id)
                        }
                    }
                },
                {
                    view : 'multiview',
                    id : 'view',
                    cells : [
                        {
                            id : 'welcome',
                            template: ''
                        }
                    ]
                }
            ]}
    ]
}
