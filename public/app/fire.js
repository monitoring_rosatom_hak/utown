
webix.protoUI({
    name : 'fire',
    $init : function (config) {
        var wnd = this
        config.rows = [
            {
                view : 'template',
                template : '<h2><i class="fas fa-fire-extinguisher"></i>Пожары</h2>',
                autoheight : true,
            },
            {
                view : 'datatable',
                id : 'data',
                select : 'row',
                columns : [
                    {id:'id',hidden:true},
                    {id:'lat',header:'Широта'},
                    {id:'lon',header:'Долгота'},
                    {id:'dot_name',header:'Описание точки',fillspace: true},
                    {id:'created_at',header:'Дата',width:250,}
                ]
            }
        ]

        this.$ready.push(this._init);
    },

    _init : function(){

        var wnd = this;
        webix.extend(wnd.$$("data"),webix.ProgressBar)
    },

    load : function () {

        var wnd = this

        wnd.$$("data").showProgress({
            type:"icon"
        })

        webix.ajax().get('/issues/fire').then(function (data) {
            wnd.$$('data').parse(data.json())
        })

    }


},webix.IdSpace,webix.ui.layout)