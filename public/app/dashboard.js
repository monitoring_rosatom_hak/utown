
webix.protoUI({
    name : 'dashboard',

    $init : function (config) {
        var wnd = this
        config.rows = [
            {
                view : 'template',
                template : '<h1>Мониторинг</h1>',
                id : 'board'
                //autoheight: true
            }
        ]

        this.$ready.push(this._init);
    },
    
    _init : function () {
        var wnd = this

        webix.ajax().get('/issues').then(function (data) {
            var val = data.text()

            wnd.$$('board').define("template",val);
            wnd.$$('board').refresh();

        })
    }

},webix.IdSpace,webix.ui.layout)