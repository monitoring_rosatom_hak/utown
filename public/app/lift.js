
webix.protoUI({
    name : 'lift',
    $init : function (config) {
        var wnd = this
        config.rows = [
            {
                view : 'template',
                template : '<h2>Инциденты с лифтами</h2>',
                autoheight : true,
            },
            {
                view : 'datatable',
                id : 'data',
                select : 'row',
                columns : [
                    {id:'id',hidden:true},
                    {id:'lat',header:'Широта'},
                    {id:'lon',header:'Долгота'},
                    {id:'dot_name',header:'Описание точки',fillspace: true},
                    {id:'floor',header:'Этаж'},
                    {id:'status',header:'Статус',width:320},
                    {id:'created_at',header:'Дата',width:250,}
                ]
            }
        ]

        this.$ready.push(this._init);
    },

    _init : function(){

        var wnd = this;
        webix.extend(wnd.$$("data"),webix.ProgressBar)
    },

    load : function () {

        var wnd = this

        wnd.$$("data").showProgress({
            type:"icon"
        })

        webix.ajax().get('/issues/lift').then(function (data) {
            wnd.$$('data').parse(data.json())
        })

    }


},webix.IdSpace,webix.ui.layout)